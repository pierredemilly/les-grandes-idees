var readingTime = require('reading-time');

var searchClient = algoliasearch(
  "U3CYRY5OB9",
  "698d675cb7d68bd70fef1ea5dde80e2b" // search only API key, no ADMIN key
);

var search = instantsearch({
  indexName: "Answer",
  searchClient: searchClient,
  routing: true
});

search.addWidget(
  instantsearch.widgets.configure({
    hitsPerPage: 7
  })
);

search.addWidget(
  instantsearch.widgets.searchBox({
    container: "#search-box",
    placeholder: "Chercher parmi les réponses marquantes du Grand Débat",
    autofocus: true
  })
);

search.addWidget(
  instantsearch.widgets.refinementList({
    container: '#themes',
    attribute: 'question_theme',
    operator: 'or',
  })
);

search.addWidget(
  instantsearch.widgets.refinementList({
    container: '#contributors',
    attribute: 'contributor_type',
    operator: 'and',
  })
);

search.addWidget(
  instantsearch.widgets.refinementList({
    container: '#zipcodes',
    attribute: 'zipcode',
    showMore: true,
    searchable: true,
    searchablePlaceholder: 'Chercher un code postal...',
  })
);

let lastRenderArgs;

const infiniteHits = instantsearch.connectors.connectInfiniteHits(
  async (renderArgs, isFirstRender) => {
    const { hits, showMore, widgetParams } = renderArgs;
    const { container } = widgetParams;

    lastRenderArgs = renderArgs;

    if (isFirstRender) {
      const sentinel = document.createElement('div');
      const div = document.createElement('div');
      div.className = 'ais-Hits';
      const ol = document.createElement('ol');
      ol.className = 'ais-Hits-list'
      div.appendChild(ol);

      container.appendChild(div);
      container.appendChild(sentinel);

      const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          if (entry.isIntersecting && !lastRenderArgs.isLastPage) {
            showMore();
          }
        });
      });

      observer.observe(sentinel);

      return;
    }

    container.querySelector('ol').innerHTML = (await Promise.all(hits
      .map(async (hit) => {
        let villes = await fetch(
          `https://geo.api.gouv.fr/communes?codePostal=${hit.zipcode}`,
          {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          }
        ).then((res) => res.json())

        const s_villes = villes.sort((a, b) => a.population >= b.population)

        const location = villes.length > 0 ?
          `${s_villes[0].nom}, ${hit.zipcode}` :
          hit.zipcode

        let minutes = readingTime(hit.content).minutes
        let seconds = Math.floor((minutes - Math.floor(minutes)) * 4) * 15

        if (seconds == 60) {
          minutes += 1
          seconds = null
        }

        minutes = Math.floor(minutes)

        let message = ''

        if (minutes > 1) {
          message += `${minutes}'`
        } else if (minutes == 1) {
          message += `${minutes}'`
        } else if (minutes == 0) {
          if (seconds == 0) {
            message += `15''`
          }
        }

        if (seconds > 0) {
          message += `${seconds}''`
        }

        return `<li class="ais-Hits-item result-item">
          <div class="result">
            <div class="theme ${hit.question_theme.substring(0,2)}"></div>
            <div class="question">
              ${instantsearch.highlight({ attribute: 'question_text', hit })}
            </div>
            <div class="info">
              <span class="tag"><i class="fas fa-user-check"></i> ${hit.flag_count}</span>
              <span class="tag"><i class="far fa-clock"></i> ${message}</span>
              ${
                location != hit.zipcode ?
                  `<a href="https://www.openstreetmap.org/search?query=${encodeURI(`${location}, France`)}">
                    <span class="tag">
                      <i class="fas fa-map-marker-alt"></i> ${location}
                    </span>
                  </a>` :
                  `<span class="tag">
                    <i class="fas fa-map-marker-alt"></i> ${location}
                  </span>`
              }
            </div>
            <div class="answer">
              ${instantsearch.highlight({ attribute: 'content', hit })}
            </div>
            <div class="controls">
              <div class="read">Voir les autres réponses de cette personne</div>
            </div>
          </div>
        </li>`
        }
      )))
      .join('');
  }
);

search.addWidget(
  infiniteHits({
    container: document.querySelector('#hits'),
  })
);

search.start();
