# Les Grandes Idéées

Cette application a été dévéloppée lors du Hackathon organisé à l'Assemblée Nationale Française le 24 Mars 2019 à la suite du Grand Débat National.

## Installation

Il s'agit d'une application Rails 5.2.2 et Ruby 2.4.4.
Il vous faut également Postgresql en local.

Pour démarrer l'application :

```
// Installe les gems nécessaires
gem install bundle
bundle install

// Installe les packages NPM
yarn install

// Crée la base de données
bin/rails db:setup
bin/rails db:migrate

// démarre le serveur
rails s
```

Une fois le serveur Rails démarré, vous pouvez également lancer le serveur de compilation d'asset Webpack.

```
./bin/webpack-dev-server
```